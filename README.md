# Memorias de tradución

As memorias de tradución (MT) son compilacións de traducións
que se empregan para facilitar a tradución en programas de
asistencia á tradución por computador (TAC, ou CAT, Computer
Aided Translation polas súas siglas en ingleś) ao forneceren
propostas de tradución para textos xa traducidos semellantes
aos textos que se están a traducir.

Do mesmo xeito que outros formatos relacionados coa
tradución teñen os seus propios formatos estándares, as
memorias de tradución teñen o seu propio formato, chamado
[TMX (Translation Memory eXchange)](https://en.wikipedia.org/wiki/Translation_Memory_eXchange).

Ao longo dos moitos anos de traballo do Proxecto Trasno
traducíronse multitude de programas e proxectos de software
libre, o que implicou a localización de centos de miles de cadeas
de texto. Dado que ese gran número de traducións pode ser de
grande axuda, desde o Proxecto Trasno queremos poñer á
disposición de todas aquelas persoas interesadas as memorias
de tradución en formato TMX que se xeraron para algúns dos
proxectos, programas e documentación que se traduciu no
Proxecto Trasno, ou noutros organismos ou empresas
relacionadas co software libre.
