#!/usr/bin/env python3

import json
import os
import re
from shutil import rmtree
from subprocess import run, PIPE
from sys import argv, modules
from urllib.parse import urljoin

import dateparser
import requests
from parsel import Selector
from transifex.api import transifex_api
from transifex.api.jsonapi.exceptions import DoesNotExist

from clean import main as clean


def download_from_damned_lies(*, slug=None, pattern=None, dest):
    if pattern:
        response = requests.get("https://l10n.gnome.org/releases/json")
        data = response.json()
        items = [
            item for item in data
            if re.search(pattern, item['fields']['description'])
        ]
        if len(items) != 1:
            raise RuntimeError(
                f"Expected 1 item in https://l10n.gnome.org/releases/json "
                f"with a description matching {pattern!r}, got {len(items)}."
            )
        slug = items[0]['fields']['name']
    parts = ["ui"]
    if slug not in ("incubator", "librem"):
        parts.append("doc")
    for part in parts:
        link = f"https://l10n.gnome.org/languages/gl/{slug}/{part}.tar.gz"
        archive_name = f"{slug}-{part}.tar.gz"
        run(["curl", "-L", link, "-o", f"{dest}/{archive_name}"])
        run(["tar", "-xavf", f"{dest}/{archive_name}", "-C", f"{dest}/"])
        os.unlink(f"{dest}/{archive_name}")


def download_from_transifex(org, *, dest=None, needs_xliff=False):
    if "TRANSIFEX_API_KEY" not in os.environ:
        print(
            "Erro: Para actualizar unha memoria dunha tradución feita en "
            "Transifex ten que definir a variábel de ambiente "
            "TRANSIFEX_API_KEY coa súa chave da API de Transifex, que pode "
            "xerar desde https://app.transifex.com/user/settings/api/"
        )
        return

    if dest is None:
        dest = org

    config = (
        "[main]\n"
        "host = https://app.transifex.com\n"
        "\n"
    )

    transifex_api.setup(auth=os.environ["TRANSIFEX_API_KEY"])
    try:
        organization = transifex_api.Organization.get(slug=org)
    except DoesNotExist:
        raise RuntimeError(
            f"Non se atopou a organización {org} en Transifex. Igual ten que "
            f"unirse a ela primeiro para ter acceso?"
        )

    os.mkdir(dest)
    os.mkdir(f"{dest}/.tx")

    projects = organization.fetch('projects')
    for project in projects:
        resources = project.fetch('resources')
        for resource in resources:
            i18n_type = resource.attributes['i18n_type']
            if i18n_type == "PO":
                extension = "po"
            elif i18n_type == "QT":
                extension = "ts"
            elif i18n_type in ("ANDROID", "DESKTOP", "KEYVALUEJSON", "STRINGS"):
                # TODO: Support these.
                continue
            else:
                raise ValueError(f"Unsupported i18n type: {i18n_type}")
            config += (
                f"[{resource.id}]\n"
                f"file_filter = {resource.attributes['slug']}.{extension}\n"
                f"resource_name = {resource.attributes['name']}\n"
                "\n"
            )
    with open(f"{dest}/.tx/config", "w") as output:
        output.write(config)

    # Algúns proxectos, como Nextcloud, necesitan forzar a descarga a XLIFF
    # porque senón descargan algúns ficheiros en formato distinto de PO.
    if needs_xliff:
        run(["tx", "pull", "-l", "gl", "--xliff"], cwd=dest)
        result = run(["find", ".", "-name", "*.xlf"], cwd=dest, stdout=PIPE)
        for line in result.stdout.splitlines():
            source = line.decode()[2:]
            target = source[:-4]
            run(["xliff2po", source, target], cwd=dest)
            run(["sed", "-i", "/^#,/d", target], cwd=dest)
            os.unlink(f"{dest}/{source}")
    else:
        run(["tx", "pull", "-l", "gl"], cwd=dest)
        result = run(["find", ".", "-name", "*.ts"], cwd=dest, stdout=PIPE)
        for line in result.stdout.splitlines():
            source = line.decode()[2:]
            target = source[:-3] + ".po"
            run(["ts2po", source, target], cwd=dest)
            os.unlink(f"{dest}/{source}")

    run(["po2tmx", dest, f"{dest}.tmx", "-l", "gl"])
    rmtree(dest)


def download_from_weblate(*, domain, dest, projects=None, exclude=tuple()):
    os.mkdir(dest)
    if not projects:
        projects = []
        response = requests.get(f"https://{domain}/languages/gl/")
        selector = Selector(text=response.text)
        for url in selector.css(".object-link ::attr(href)").getall():
            project = url.rstrip("/").split("/")[-3]
            if project in exclude:
                continue
            projects.append(project)
    for project in projects:
        link = f"https://{domain}/download/{project}/-/gl/?format=zip"
        run(["curl", "-L", link, "-o", f"{dest}/{project}.zip"])
        run(["unzip", f"{dest}/{project}.zip", "-d", f"{dest}/"])
        os.unlink(f"{dest}/{project}.zip")

    result = run(["find", ".", "-name", "gl.po"], cwd=dest, stdout=PIPE)
    for line in result.stdout.splitlines():
        source = line.decode()[2:]
        target = source.replace("/", "-")
        os.rename(f"{dest}/{source}", f"{dest}/{target}")
    result = run(["find", ".", "-type", "d", "-name", "gl"], cwd=dest, stdout=PIPE)
    for line in result.stdout.splitlines():
        folder = line.decode()[2:]
        _result = run(["find", folder, "-name", "*.po"], cwd=dest, stdout=PIPE)
        for _line in _result.stdout.splitlines():
            source = _line.decode()
            target = source.replace("/", "-")
            os.rename(f"{dest}/{source}", f"{dest}/{target}")
    result = run(
        [
            "find",
            ".",
            "-mindepth",
            "1",
            "-maxdepth",
            "1",
            "-type",
            "d",
            "-exec",
            "rm",
            "-rf",
            "{}",
            ";",
        ],
        cwd=dest,
    )
    run(["po2tmx", dest, f"{dest}.tmx", "-l", "gl"])
    rmtree(dest)


def download_from_wordpress(category, project, *, dest):
    project_url = f"https://translate.wordpress.org/locale/gl/default/{category}/{project}/"
    response = requests.get(project_url)
    selector = Selector(response.text)
    for url in selector.css(".set-name ::attr(href)").getall():
        link = urljoin(project_url, url)
        link = urljoin(link, "./export-translations/")
        response = requests.get(link)
        if response.status_code != 200:
            continue
        slug = url.strip("/").replace("/", "-")
        with open(f"{dest}/{slug}.po", "wb") as output:
            output.write(response.content)


def update_archlinux():
    download_from_transifex("toofishes", dest="archlinux")


def update_askbot():
    download_from_transifex("askbot")


def update_backintime():
    download_from_weblate(
        domain="translate.codeberg.org",
        dest="backintime",
        projects=["backintime"],
    )


def update_canonical():
    os.mkdir("canonical")

    start_url = "https://translations.launchpad.net/ubuntu/+translations"
    response = requests.get(start_url)
    selector = Selector(text=response.text)
    latest_url = selector.css(".top-portlet ::attr(href)").get()
    latest_url = urljoin(start_url, latest_url)
    ending = "/+translations"
    if not latest_url.endswith(ending):
        raise RuntimeError(f"Expected {latest_url!r} to end in {ending!r}")
    latest_url = latest_url[:-len(ending)] + "/+lang/gl/+index?batch=300"

    seen_slugs = set()
    canonical_slugs = set()
    while True:
        response = requests.get(latest_url)
        selector = Selector(text=response.text)
        for template_url in selector.css(".template-name ::attr(href)").getall():
            # Packages that are Ubuntu-specific do not have a importer/debian/dsc
            # branch.
            slug = re.search(r"/\+source/([^/]+)", template_url)[1]
            if slug in seen_slugs:
                continue
            seen_slugs.add(slug)
            deb_branch_url = (
                f"https://code.launchpad.net/~git-ubuntu-import/ubuntu/+source"
                f"/{slug}/+git/{slug}/+ref/importer/debian/dsc"
            )
            response = requests.get(deb_branch_url)
            if response.status_code == 200:
                continue

            link = f"https://git.launchpad.net/ubuntu/+source/{slug}/plain/po/gl.po"
            response = requests.get(link, allow_redirects=False)
            if response.status_code != 200:
                # Bad luck, upstream does not actually keep the translation as
                # a PO file. Seen with .desktop files.
                continue
            with open(f"canonical/{slug}.po", "wb") as output:
                output.write(response.content)

        latest_url = selector.css("a.next ::attr(href)").get()
        if not latest_url:
            break

    run(["po2tmx", "canonical", "canonical.tmx", "-l", "gl"])
    rmtree("canonical")


def update_ckeditor():
    download_from_transifex("ckeditor")


def update_debian():
    os.mkdir("debian")

    response = requests.get("https://www.debian.org/international/l10n/po-debconf/gl")
    selector = Selector(text=response.text)
    xpath = "//tr[not(contains(td[2]/text(), '(0t'))]//@href[contains(., '.po.gz')]"
    for url in selector.xpath(xpath).getall():
        name = os.path.basename(url)
        run(["curl", url, "-o", f"debian/{name}"])
        run(["gunzip", f"debian/{name}"])

    # https://d-i.debian.org/doc/i18n-guide/ch01.html
    repos = [
        "installer-team/d-i",
        "installer-team/tasksel",
        "debian/iso-codes",
        "popularity-contest-team/popularity-contest",
        "debian/util-linux",
        "a11y-team/espeakup",
        "grub-team/grub",
        "pkg-debconf/debconf",
        "mckinstry/newt",
        "installer-team/win32-loader",
    ]
    for repo in repos:
        run(
            [
                "git",
                "clone",
                "--depth=1",
                f"https://salsa.debian.org/{repo}.git",
            ],
            cwd="debian",
        )
    result = run(["find", ".", "-name", "gl.po"], cwd="debian", stdout=PIPE)
    for line in result.stdout.splitlines():
        source = line.decode()[2:]
        target = source.replace("/", "-")
        os.rename(f"debian/{source}", f"debian/{target}")
    result = run(
        [
            "find",
            ".",
            "-mindepth",
            "1",
            "-maxdepth",
            "1",
            "-type",
            "d",
            "-exec",
            "rm",
            "-rf",
            "{}",
            ";",
        ],
        cwd="debian",
    )

    run(["po2tmx", "debian", "debian.tmx", "-l", "gl"])
    rmtree("debian")


def update_drupal():
    os.mkdir("drupal")

    base_url = "https://ftp.drupal.org/files/translations/all/"
    response = requests.get(base_url)
    selector = Selector(text=response.text)
    outer_xpath = "//a[@href!='../'][@href!='all']/@href"
    inner_xpath = "//a[re:test(@href, '\\.gl\\.po$')]"
    for url in selector.xpath(outer_xpath).getall():
        latest_link = latest_date = None
        url = urljoin(base_url, url)
        response = requests.get(urljoin(base_url, url))
        selector = Selector(text=response.text)
        for a in selector.xpath(inner_xpath):
            date_str = a.xpath("following-sibling::text()[1]").get()
            date_str = date_str.strip().split("  ", maxsplit=1)[0]
            date = dateparser.parse(date_str, languages=["en"])
            if date is None:
                raise ValueError(f"Could not parse date {date_str}")
            if latest_link is None or date > latest_date:
                latest_link = a.css("::attr(href)").get()
                latest_date = date
        if latest_link is not None:
            response = requests.get(urljoin(url, latest_link))
            with open(f"drupal/{latest_link}", "wb") as output:
                output.write(response.content)

    run(["po2tmx", "drupal", "drupal.tmx", "-l", "gl"])
    rmtree("drupal")

def update_fedora():
    download_from_weblate(
        domain="translate.fedoraproject.org",
        dest="fedora",
    )


def update_freedesktop_org():
    os.mkdir("freedesktop-org")
    download_from_damned_lies(slug="freedesktop-org", dest="freedesktop-org")
    run(["po2tmx", "freedesktop-org", "freedesktop-org.tmx", "-l", "gl"])
    rmtree("freedesktop-org")


def update_gnome():
    os.mkdir("gnome")
    download_from_damned_lies(pattern=r"^GNOME\s*\d+\s*\(stable\)$", dest="gnome")
    slugs = (
        "incubator",
        "gnome-circle",
        "gnome-infrastructure",
        "evolution",
        "gnome-gimp",
    )
    for slug in slugs:
        download_from_damned_lies(slug=slug, dest="gnome")
    run(["po2tmx", "gnome", "gnome.tmx", "-l", "gl"])
    rmtree("gnome")


def update_kde():
    run(["svn", "checkout", "svn://anonsvn.kde.org/home/kde/trunk/l10n-support/gl/summit", "kde"])
    run(["po2tmx", "kde", "kde.tmx", "-l", "gl"])
    rmtree("kde")


def update_midnight_commander():
    response = requests.get("https://github.com/MidnightCommander/mc/raw/master/po/gl.po")
    with open("midnight-commander.po", "wb") as output_file:
        output_file.write(response.content)
    run(["po2tmx", "midnight-commander.po", "midnight-commander.tmx", "-l", "gl"])
    os.unlink("midnight-commander.po")


def update_mozilla():
    run(["curl", "https://pontoon.mozilla.org/translation-memory/gl.all-projects.tmx", "-o", "mozilla.tmx"])


def update_nextcloud():
    download_from_transifex("nextcloud", needs_xliff=True)


def update_opensuse():
    download_from_weblate(
        domain="l10n.opensuse.org",
        dest="opensuse",
        exclude=(
            "libstorage",  # → libstorage-ng
        )
    )


def update_purism():
    os.mkdir("purism")
    download_from_damned_lies(slug="librem5", dest="purism")
    run(["po2tmx", "purism", "purism.tmx", "-l", "gl"])
    rmtree("purism")


def update_readthedocs():
    download_from_transifex("readthedocs")


def update_smplayer():
    download_from_transifex("rvm", dest="smplayer")


def update_tdf():
    if "TDF_WEBLATE_API_KEY" not in os.environ:
        print(
            "Erro: Para actualizar a memoria de tradución de TDF ten que "
            "definir a variábel de ambiente TDF_WEBLATE_API_KEY coa súa chave "
            "da API do Weblate de TDF que atopará en "
            "https://translations.documentfoundation.org/accounts/profile/#api"
        )
        return
    os.mkdir("tdf")
    api_key = os.environ["TDF_WEBLATE_API_KEY"]
    result = run(
        [
            "wlc",
            "-u",
            "https://translations.documentfoundation.org/api/",
            "-k",
            api_key,
            "--format",
            "json",
            "list-components",
        ],
        stdout=PIPE
    )
    components = json.loads(result.stdout)
    exclude_project_pattern = r"\d\.\d|LibreOffice Online"
    for component in components:
        if re.search(exclude_project_pattern, component["project"]["name"]):
            continue
        print(f"{component['project']['slug']}/{component['slug']}…")
        result = run(
            [
                "wlc",
                "-u",
                "https://translations.documentfoundation.org/api/",
                "-k",
                api_key,
                "download",
                f"{component['project']['slug']}/{component['slug']}/gl",
                "-c",
                "po",
                "-o",
                f"tdf/{component['project']['slug']}-{component['slug']}.po",
            ],
            stderr=PIPE,
        )
    run(["po2tmx", "tdf", "tdf.tmx", "-l", "gl"])
    rmtree("tdf")


def update_sphinx_doc():
    download_from_transifex("sphinx-doc")

def update_weblate():
    download_from_weblate(
        domain="hosted.weblate.org",
        dest="weblate",
        projects=["weblate"],
    )


def update_wesnoth():
    os.mkdir("wesnoth")

    run(
        [
            "git",
            "clone",
            "--depth=1",
            f"https://github.com/wesnoth/wesnoth.git",
        ],
        cwd="wesnoth",
    )
    result = run(["find", ".", "-name", "gl.po"], cwd="wesnoth/wesnoth/po", stdout=PIPE)
    for line in result.stdout.splitlines():
        source = line.decode()[2:]
        target = source.replace("/", "-")
        os.rename(f"wesnoth/wesnoth/po/{source}", f"wesnoth/{target}")
    rmtree("wesnoth/wesnoth")
    run(["po2tmx", "wesnoth", "wesnoth.tmx", "-l", "gl"])
    rmtree("wesnoth")


def update_wordpress():
    os.mkdir("wordpress")

    download_from_wordpress("wp", "dev", dest="wordpress")

    for category in ("themes", "plugins"):
        response = requests.get(f"https://translate.wordpress.org/locale/gl/default/stats/{category}/")
        selector = Selector(text=response.text)
        xpath = "//tr[td[@data-column-title='Translated'][@data-sort-value!='0']]/th//@href"
        for url in selector.xpath(xpath).getall():
            url = url.strip("/")
            _, category, project = url.split("/")
            download_from_wordpress(category, project, dest="wordpress")

    download_from_wordpress("patterns", "core", dest="wordpress")

    for category in ("meta", "apps"):
        response = requests.get(f"https://translate.wordpress.org/locale/gl/default/{category}/")
        selector = Selector(text=response.text)
        for url in selector.css(".project-name ::attr(href)").getall():
            url = url.strip("/")
            _, category, project = url.rsplit("/", maxsplit=2)
            download_from_wordpress(category, project, dest="wordpress")

    run(["po2tmx", "wordpress", "wordpress.tmx", "-l", "gl"])
    rmtree("wordpress")


SLOW_SET = {"drupal"}


def main():
    if len(argv) >= 2:
        project = argv[1]
        if project.endswith(".tmx"):
            project = project[:-4]
        getattr(modules[__name__], f"update_{project.replace('-', '_')}")()
        clean(f"{project}.tmx")
    else:
        fail_set = set()
        for name, func in modules[__name__].__dict__.items():
            if not name.startswith("update_"):
                continue
            project = name[7:].replace("_", "-")
            if project in SLOW_SET:
                print(f"Skipping {project} (slow)…")
                continue
            print(f"Updating {project}…")
            try:
                func()
            except Exception as e:
                fail_set.add(project)
            else:
                clean(f"{project}.tmx")
        if fail_set:
            print(f"Could not update: {', '.join(sorted(fail_set))}.")

if __name__ == "__main__":
    main()
