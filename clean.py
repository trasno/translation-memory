#!/usr/bin/env python3

import os
from sys import argv

import lxml.etree as etree

def clean_1_4(tree, file_name):
    seen = set()
    body = tree.xpath("//body")[0]
    for unit in tree.xpath("//tu"):
        source_tuv = unit.xpath(".//tuv[contains(@xml:lang, 'en') or contains(@xml:lang, 'EN')]")[0]
        source_tuv.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "en"
        source = "".join(source_tuv.xpath("seg/text()"))
        try:
            target_tuv = unit.xpath(".//tuv[@xml:lang!='en']")[0]
        except IndexError:
            body.remove(unit)
            continue
        target = "".join(target_tuv.xpath("seg/text()"))
        pair = (source, target)
        if pair not in seen:
            target_tuv.attrib['{http://www.w3.org/XML/1998/namespace}lang'] = "gl"
            seen.add(pair)
            continue
        body.remove(unit)
    if tree.xpath("//tu"):
        with open(file_name, "w") as output_file:
            output_file.write(
                etree.tostring(
                    tree,
                    method="xml",
                    doctype=(
                        '<?xml version="1.0" encoding="UTF-8"?>\n'
                        '<!DOCTYPE tmx SYSTEM "tmx14.dtd">'
                    ),
                    encoding="unicode",
                ),
            )
            output_file.write("\n")
    else:
        os.unlink(file_name)

def clean_1_1(tree, file_name):
    seen = set()
    body = tree.xpath("//body")[0]
    for unit in tree.xpath("//tu"):
        source_tuv = unit.xpath(".//tuv[contains(@lang, 'en') or contains(@lang, 'EN')]")[0]
        source_tuv.attrib['lang'] = "en"
        source = source_tuv.xpath("seg/text()")[0]
        try:
            target_tuv = unit.xpath(".//tuv[@lang!='en']")[0]
        except IndexError:
            body.remove(unit)
            continue
        target = target_tuv.xpath("seg/text()")[0]
        pair = (source, target)
        if pair not in seen:
            target_tuv.attrib['lang'] = "gl"
            seen.add(pair)
            continue
        body.remove(unit)
    if tree.xpath("//tu"):
        with open(file_name, "w") as output_file:
            output_file.write(
                etree.tostring(
                    tree,
                    method="xml",
                    doctype=(
                        '<?xml version="1.0" encoding="UTF-8"?>\n'
                        '<!DOCTYPE tmx SYSTEM "tmx11.dtd">'
                    ),
                    encoding="unicode",
                ),
            )
            output_file.write("\n")
    else:
        os.unlink(file_name)

CLEANERS = {
    "1.4": clean_1_4,
    "1.1": clean_1_1,
}

def main(file_name):
    with open(file_name) as input_file:
        tree = etree.parse(input_file)
    tmx_version = tree.xpath("//tmx")[0].attrib["version"]
    CLEANERS[tmx_version](tree, file_name)


if __name__ == "__main__":
    main(argv[1])
